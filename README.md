# Node By Term

Node By Term module allows you to easily filter all published nodes based on
their usage of vocabularies and taxonomy terms. When you choose a specific
vocabulary, the module will display the associated taxonomy terms, which you can
then use to filter the nodes. Additionally, you have the ability to filter nodes
by their content type. This module provides a convenient way to sort and manage
your nodes according to these criteria.

For a full description of the module, visit the project page:
[project page](https://www.drupal.org/project/node_by_term)

To submit bug reports and feature suggestions, or to track changes:
[issue queue](https://www.drupal.org/project/issues/node_by_term)


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- The module has no menu or modifiable settings. There is no configuration.
- After Installation, You will get one message "Go to nodelist" by clicking it
  you will go to that page "/nodelist".


## Maintainers

- [Bhupendra_Raykhere](https://www.drupal.org/u/bhupendra_raykhere)
- [Priyansh Chourasiya](https://www.drupal.org/u/priyansh-chourasiya)
