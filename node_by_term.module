<?php

/**
 * @file
 * Implements Node By Term module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_permission().
 */
function node_by_term_permission() {
  return [
    'administer node by term settings' => [
      'title' => t('Administer node by term settings'),
      'description' => t('Access and manage the settings for node by term Module.'),
    ],
  ];
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function node_by_term_form_node_form_alter(&$form, $form_state, $form_id) {
  // Add a custom submit handler to the form.
  $form['actions']['submit']['#submit'][] = 'node_by_term_node_form_submit';
}

/**
 * Custom submit handler for the node form.
 */
function node_by_term_node_form_submit($form, $form_state) {
  // Set a redirect to the node list page after the form is submitted.
  $form_state->setRedirect('node_by_term.nodelist');
}

/**
 * Implements hook_help().
 */
function node_by_term_help($route_name, RouteMatchInterface $route_match)
{
  switch ($route_name) {
    case 'help.page.node_by_term':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('With the help of this module we can fetch all our nodes on the basis of taxonomy term and vocablary and content type.') . '</p>';
      $output .= '<p>' . t(
        'For more information, see the <a href=":handbook">
      online documentation for the Node By Term </a>.',
        [
          ':node_by_term' => Url::fromRoute('help.page', ['name' => 'node_by_term'])->toString(),
          ':handbook' => 'https://www.drupal.org/project/node_by_term'
        ]
      ) . '</p>';
      $output .= '<h3>' . t('Features') . '</h3>';
      $output .= '<p>' . t('The key feature of this module is that it allows you to easily filter all published nodes based on their usage of vocabularies and taxonomy terms. When you choose a specific vocabulary, the module will display the associated taxonomy terms, which you can then use to filter the nodes. Additionally, you have the ability to filter nodes by their content type. This module provides a convenient way to sort and manage your nodes according to these criteria.');
      $output .= '</p>';
      return $output;

    default:
  }
}