<?php

namespace Drupal\node_by_term\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Link;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

/**
 * Filter Node by Vocabulary and terms.
 */
class NodeListController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  protected $requestStack;

  /**
   * Constructs a new EntityActionDeriverBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter, Connection $database, FormBuilderInterface $form_builder, RequestStack $requestStack) {
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
    $this->database = $database;
    $this->formBuilder = $form_builder;
    $this->requestStack = $requestStack;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('database'),
      $container->get('form_builder'),
      $container->get('request_stack')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function nodelist() {
    $data['filter'] = $this->formBuilder->getForm('Drupal\node_by_term\Form\NodeByTermForm');
    $request = $this->requestStack->getCurrentRequest();
    $vocab = $request->query->get('vocab');
    $t_id = $request->query->get('t_id');
    $content_type = $request->query->get('cont_type');
    if (!empty($vocab) and  empty($t_id) and empty($content_type)) {
      $query = $this->database->select('taxonomy_index', 'ti');
      $query->join('node_field_data', 'nfd', 'nfd.nid = ti.nid');
      $query->fields('nfd', ['created', 'changed']);
      $query->fields('ti', ['nid']);
      $query->condition('ti.tid', $t_id, 'IN');

      // For the pagination we need to extend the pagerselectextender.
      $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(2);
      $nids = $pager->execute()->fetchall();

      $nid = [];
      foreach ($nids as $ids) {
        $nid[] = $ids->nid;
      }
      $nodes = $this->entityTypeManager()->getStorage('node')->loadMultiple($nid);
    }
    elseif (!empty($vocab and $t_id)) {
      $query = $this->database->select('taxonomy_index', 'ti');
      $query->join('node_field_data', 'nfd', 'nfd.nid = ti.nid');
      $query->fields('nfd', ['created', 'changed']);
      $query->fields('ti', ['nid']);
      $query->condition('ti.tid', $t_id);
      // Add the condition on the content type, if content type is set.
      if (isset($content_type) && !empty($content_type)) {
        $query->condition('nfd.type', $content_type);
      }
      // For the pagination we need to extend the pagerselectextender.
      $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(2);
      $nids = $pager->execute()->fetchall();
      $nid = [];
      foreach ($nids as $ids) {
        $nid[] = $ids->nid;
      }
      $nodes = $this->entityTypeManager()->getStorage('node')->loadMultiple($nid);
    }
    else {
      $query = $this->database->select('node_field_data', 'nfd');
      $query->fields('nfd', ['nid']);
      $query->addField('nfd', 'created');
      $query->addField('nfd', 'changed');
      if (isset($content_type) && !empty($content_type)) {
        $query->condition('nfd.type', $content_type);
      }
      // For the pagination we need to extend the pagerselectextender.
      $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(2);
      $nids = $pager->execute()->fetchCol();
      $nid = [];
      foreach ($nids as $ids) {
        $nid[] = $ids;
      }
      $nodes = $this->entityTypeManager()->getStorage('node')->loadMultiple($nid);
    }


    // Prepare the header for the table.
    $header = [
      'title' => $this->t('Node Title'),
      'content_type' => $this->t('Content Type'),
      'status' => $this->t('Published Status'),
      'created-date' => $this->t('Created date'),
      'last modified date' => $this->t('Last modified'),
      'Operations' => $this->t('Operations'),
    ];
    // Prepare the rows for the table.
    $rows = [];

    foreach ($nodes as $node) {
      // Format dates.
      $created_date = $this->dateFormatter->format($node->getCreatedTime(), 'custom', 'd-m-Y H:i:s');
      $changed_date = $this->dateFormatter->format($node->getChangedTime(), 'custom', 'd-m-Y H:i:s');

      $edit_url = Url::fromRoute('entity.node.edit_form', ['node' => $node->id()])->toString();
      $edit_link = new FormattableMarkup('<a href=":link">:label</a>',
      [':link' => $edit_url, ':label' => $this->t('Edit')]);
      // Create "View" link.
      $view_url = Url::fromRoute('entity.node.canonical', ['node' => $node->id()]);
      $view_link = Link::fromTextAndUrl($this->t('View'), $view_url)->toString();
      // Render the links.
      $links = new FormattableMarkup('@edit_link | @view_link',
      ['@edit_link' => $edit_link, '@view_link' => $view_link]);

      $rows[] = [
        'title' => $node->getTitle(),
        'content_type' => $node->getType(),
        'status' => $node->isPublished() ? $this->t('Published') : $this->t('Unpublished'),
        'created-date' => $created_date,
        'last modified date' => $changed_date,
        'edit' => $links,
      ];
    }

    // Build the table render array.
    $data['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No nodes founds.'),
    ];
    $data['pager'] = [
      '#type' => 'pager',
    ];
    // Prevent caching.
    $data['#cache']['max-age'] = 0;

    return $data;
  }

}
