<?php

namespace Drupal\node_by_term\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to filter nodes.
 */
class NodeByTermForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * Constructs a new EntityActionDeriverBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\taxonomy\VocabularyStorageInterface $vocabulary_storage
   *   Vocabulary storage.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_by_term_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get elements from query parameter.
    $vocab = $this->getRequest()->query->get('vocab');
    $t_id = $this->getRequest()->query->get('t_id');
    $content_type_value = $this->getRequest()->query->get('cont_type');

    // This will load all vocabulary.
    // Get the EntityTypeManager service.
    $entity_type_manager = \Drupal::entityTypeManager();
    // Load all vocabularies.
    $vocabularies = $entity_type_manager->getStorage('taxonomy_vocabulary')->loadMultiple();
    // Create an options array.
    $options = ['-select-'];
    foreach ($vocabularies as $vocabulary) {
      $options[$vocabulary->id()] = $vocabulary->label();
    }

    $form['vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#options' => $options,
      '#default_value' => !empty($vocab) ? $vocab : '',
      '#ajax' => [
        'callback' => '::loadTerm',
        'event' => 'change',
        'wrapper' => 'term-wrapper',
      ],
      '#attributes' => [
        'class' => ['form-control'],
      ],
    ];

    // If vocabulary is selected, load its terms.
    $selected_vocabulary = $form_state->getValue('vocabulary');

    if (!empty($selected_vocabulary)) {
      $terms = $this->entityTypeManager->getStorage('taxonomy_term')
        ->loadTree($selected_vocabulary);
      $term_options = ['-select-'];
      foreach ($terms as $term) {
        $term_options[$term->tid] = $term->name;
      }
    }
    else {
      // This condition will apply after submitting.
      $terms = $this->entityTypeManager->getStorage('taxonomy_term')
        ->loadTree($vocab);

      $term_options = ['-select-'];
      foreach ($terms as $term) {
        $term_options[$term->tid] = $term->name;
      }
    }

    $form['term'] = [
      '#type' => 'select',
      '#title' => $this->t('Term'),
      '#prefix' => '<div id="term-wrapper"  >',
      '#suffix' => '</div>',
      '#options' => !empty($term_options) ? $term_options : ['No term'] ,
      '#default_value' => !empty($t_id) ? $t_id : ' ',
      '#attributes' => [
        'class' => ['form-control'],
      ],
    ];

    // Fetch all content types.
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $content_type_options = ['-select-'];

    foreach ($content_types as $content_type) {
      $content_type_options[$content_type->id()] = $content_type->label();
    }

    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content Type'),
      '#options' => $content_type_options,
      '#default_value' => $content_type_value,
      '#attributes' => [
        'class' => ['form-control'],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => ['btn btn-success'],
      ],
    ];

    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#submit' => ['::resetForm'],
      '#attributes' => [
        'class' => ['btn btn-danger'],
      ],
    ];
    $form['#attached']['library'][] = 'node_by_term/my_styles';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function loadTerm(array $form, FormStateInterface $form_state) {
    return $form['term'];
  }

  /**
   * {@inheritdoc}
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('node_by_term.nodelist');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $vocab = $form_state->getValue('vocabulary');
    $t_id = $form_state->getValue('term');
    $cont_type = $form_state->getValue('content_type');

    $url = Url::fromRoute('node_by_term.nodelist')->setRouteParameters(
      ['vocab' => $vocab, 't_id' => $t_id, 'cont_type' => $cont_type]);
    $form_state->setRedirectUrl($url);
  }

}
